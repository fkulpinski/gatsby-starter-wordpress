module.exports = {
  plugins: [
    {
      resolve: `@gamigo/gatsby-theme-wordpress`,
      options: {
        wpGraphqlUrl: `http://wp-gatsby.gamigo.io/graphql`,
      },
    },
    {
      // See https://www.gatsbyjs.com/plugins/gatsby-plugin-manifest/?=gatsby-plugin-manifest
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gamigo WordPress Page`,
        short_name: `Gamigo Page`,
        start_url: `/`,
        background_color: `#000000`,
        theme_color: `#c8282d`,
        display: `minimal-ui`,
        icon: `content/assets/favicon-glyph.png`,
      },
    },
  ],
};
